package lesson3;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    // String q1 = new String("qwe"); // выделяет в памяти новое место
    // String q2 = new String("qwe"); // выделяет в памяти новое место
    // sout(q1 == q2); // false происходит сравнение ссылок в память
    // sout(q1.equals(q2)); // true;

    public static void main(String[] args) {

        // for (*начальное значение*; *выражение*; *порядок изменения стартового значения*) {
//        int i = 2;
        // for
//        for (int i = -20000; i < 101234; i++) { // i++ -> i = i + 1;
//            System.out.println(i);
//            // Сколько итераций будет в цикле?
//            // 101234 + 20000 => столько итераций будет в цикле
//            // [0...10) -> 10
//            // 0 1 2 3 4 5 6 7 8 9
//        }
//        for (int i = 9; i >= 0; i = i - 1) { // цикл бежит с конца до начала [9...0]
//
//        }
        // System.out.println(i);

        // Думаем над алгоритмом
        // Пишем код
        // Оптимизируем (по необходимости)

        // 1) Какие переменные (и сколько) нам потребуется
        Scanner in = new Scanner(System.in);

        // 1 <= 10000
        // byte -> -128...127
        // short -> Ok
//        short n = in.nextShort(); // 5
//        int z = 0; // 10000*10000 -> не умещается в short, int, long
//        for (short i = 0; i < n; i++) { // 4 // 10000
//            z = z + in.nextShort(); // z = 0 + 10 -> 10. z = 10 + 7 -> 17...
//        }
//        System.out.println(z);

        // forEach рассмотрим позже

        // Массивы
        // Массив -> набор однотипных элементов ("qwe", 2, 2.5)
        // Инициализация массива
        int number = in.nextInt();
        int[] ar = new int[number]; // массив - ссылочный тип данных
        // во вторых [] -> указывается размер массива (const)
        // массивы нумеруются с 0, ar[9]
        // ar[10] -> Exception (ошибка, исключение)
        // Что будет, если мы пытаемся выйти за границы массива?
        // Exception -> ArrayIndexOutBoundsException
        // По умолчанию (если мы не меняем значения массива) все заполнено 0
        System.out.println(Arrays.toString(ar)); // 1) способ вывода (в читабельном виде)

        // System.out.println(ar); // При выводе (sout) для каждого ссылочного типа данных по умолчанию вызывается метод .toString(), однако у массивов нет методов (своих) !!!
        // поведение метода .toString() по умолчанию возвращает название объекта ([I), дальше @ (разделитель) и дальше hex hashCode() -> ссылка на память
        for (int i = 0; i < ar.length; i++) { // 2) способ
            System.out.print(ar[i] + " ");
        }
        // Массив можно создавать с любым размером (int[] array = new int[-1]) -> однако такой код при компиляции сразу "упадет"
        // И массив можно создавать с размером 0 (int[] array = new int[0])
        int[] qwe2 = new int[]{1, 2, 3, 4, 5, 100, 13213, 123, 321, 5, 2};
        int[] qwe3 = {1, 2, 3, 4, 5, 12312};

        // У массивов есть поле (специальная переменная, хранящая что-либо) .length
        // qwe2.length -> размер массива
//        int[] blabla = new int[100];
        // blabla.length то же самое 100

        // Показательный момент с массивами
        int[] a1 = new int[] {1, 2, 3};
        int[] a2 = a1; // мы не клонируем массив, мы просто присваиваем ссылку
        // Мы говорим, что пускай массив a2 ссылается на тот же участок памяти, что и a1
        a2[1] = 100;
        // массивы - ссылочный тип данных!!!
        System.out.println(Arrays.toString(a1));
        System.out.println(Arrays.toString(a2));

        /* todo
            http://acm.sgu.ru/lang/problem.php?contest=2&problem=2003
            http://acm.sgu.ru/lang/problem.php?contest=2&problem=2004
         */

    }
}
