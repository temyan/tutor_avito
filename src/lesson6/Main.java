package lesson6;

import java.util.Scanner;

import static lesson5.Main.*;

public class Main {
    private String name = "Sergey";
    protected int age = 19;
    // ООП -> Объектно-ориентированного программирование
    // В качестве аналогии ООП можно рассмотреть город из людей
    // Класс - личность (человек)
    // Метод - это как функционал (возможности человека)
    // Поле (аттрибут) - это как свойство класса (имя человека, возраст человека, цвет его глаз...)

    // Модификаторы доступа:
    // public - доступ к классу/полю/методу/вложенному классу из любого места
    // protected - доступ к полю/методу/вложенному классу только внутри той же папки или от унаследованного класса
    // default (-) - доступ к классу/полю/методу/вложенному классу только внутри той же папки
    // private - доступ к полю/методу/вложенному классу только внутри данного класса

    // Возвращаемые типы (у методов)
    // void -> метод (результат метода) ничего не возвращает
    // Все остальное (int, double, String, Scanner...)

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int blabla = 123;
        System.out.println(k);
        System.out.println(nose()); // 1 variant
        int q = nose(); // 1 variant
        int noseValue = nose(); // принимаем число 10
        noseValue = noseValue + 123;
        System.out.println(noseValue);
        System.out.println(calcPositive(nose(), 99));

        String s = "Hello".substring(0, 2);
        s = s + "qwe";
        System.out.println(s);

//        Main main = new Main();
//        System.out.println(main.name);
    }

    public static void lungs() {
        System.out.println("I am lungs");
    }

    public static int nose() { // все, что до () - сигнатура метода, все, что в круглых - параметры метода
        int sum = 0;
        for (int i = 0; i < 100; i++) {
            sum += i;
        }
        return sum; // метод возвращает число 10
    }

    public static int calcPositive(int x, int y) {
        if (x <= 0 || y <= 0) {
            return 0;
        }
        // Здесь не нужно заботиться о значениях этих x, y
        // Они приходят извне
        return x + y;
    }
    public static int calcPositive(int x, String y) {
        if (x <= 0) {
            return 0;
        }
        // Здесь не нужно заботиться о значениях этих x, y
        // Они приходят извне
        return x;
    }
    public static int calcPositive(int x, int y, int z) {
        if (x <= 0 || y <= 0) {
            return 0;
        }
        // Здесь не нужно заботиться о значениях этих x, y
        // Они приходят извне
        return x + y;
    }

    /* todo
        http://acm.sgu.ru/lang/problem.php?contest=2&problem=2032
        http://acm.sgu.ru/lang/problem.php?contest=2&problem=2029
     */
}
