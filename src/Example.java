import java.util.Scanner;

public class Example {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        StringBuilder s = new StringBuilder(in.nextLine());
        // StringBuilder это специальный класс, который позволяет предоставить больше методов для String
//        for (int i = 0; i < s.length(); i++) {
//            if (s.charAt(i) != ' ') {
//                break;
//            }
//            s.deleteCharAt(i);
//        }
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i - 1) == ' ' && s.charAt(i) == ' ' ||
                    (s.charAt(i - 1) == ' ' && s.charAt(i) == '?' ||
                            s.charAt(i - 1) == ' ' && s.charAt(i) == '!' ||
                            s.charAt(i - 1) == ' ' && s.charAt(i) == '.' ||
                            s.charAt(i - 1) == ' ' && s.charAt(i) == ',')) {
                s.replace(i - 1, i, "");
                i = 1;
            }
            if (s.charAt(i - 1) == '?' && s.charAt(i) != ' ' ||
                    s.charAt(i - 1) == '!' && s.charAt(i) != ' ' ||
                    s.charAt(i - 1) == '.' && s.charAt(i) != ' ' ||
                    s.charAt(i - 1) == ',' && s.charAt(i) != ' ') {
                s.insert(i, ' ');
                i = 1;
            }
        }
        System.out.println(s);
    }
}
